#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <limits.h>

#include "common.h"

# if defined(MA1) || defined(MA2)
#   define TASK_PRIORITY_SEND(I, J, NT)   1
#   define TASK_PRIORITY_RECV(I, J, NT)   0
#   define TASK_PRIORITY_POTRF(I, J, NT)  0
#   define TASK_PRIORITY_GEMM(I, J, NT)   0
#   define TASK_PRIORITY_TRSM(I, J, NT)   0
#   define TASK_PRIORITY_SYRK(I, J, NT)   0
# elif defined(SA1)
#   define TASK_PRIORITY_SEND(I, J, NT)   (NT * NT + 1) - (I * NT + J)
#   define TASK_PRIORITY_RECV(I, J, NT)   0
#   define TASK_PRIORITY_POTRF(I, J, NT)  0
#   define TASK_PRIORITY_GEMM(I, J, NT)   0
#   define TASK_PRIORITY_TRSM(I, J, NT)   0
#   define TASK_PRIORITY_SYRK(I, J, NT)   0
# elif defined(SA2)
#   define TASK_PRIORITY_SEND(I, J, NT)   INT_MAX
#   define TASK_PRIORITY_RECV(I, J, NT)   0
#   define TASK_PRIORITY_POTRF(I, J, NT)  0
#   define TASK_PRIORITY_GEMM(I, J, NT)   0
#   define TASK_PRIORITY_TRSM(I, J, NT)   0
#   define TASK_PRIORITY_SYRK(I, J, NT)   0
# elif defined(FA1) || defined(FIFO)
#   define TASK_PRIORITY_SEND(I, J, NT)   0
#   define TASK_PRIORITY_RECV(I, J, NT)   0
#   define TASK_PRIORITY_POTRF(I, J, NT)  0
#   define TASK_PRIORITY_GEMM(I, J, NT)   0
#   define TASK_PRIORITY_TRSM(I, J, NT)   0
#   define TASK_PRIORITY_SYRK(I, J, NT)   0
# endif

# ifdef TRACE
#  define SET_LABEL(...) snprintf(label, MPC_OMP_TASK_LABEL_MAX_LENGTH, __VA_ARGS__)
# else  /* TRACE */
#  define SET_LABEL(...)
# endif /* TRACE */

void
cholesky_mpi(
        const int ts,
        const int nt,
        double * A[nt][nt],
        double * B,
        double * C[nt],
        int *block_rank)
{
    char * send_flags = malloc(sizeof(char) * np);
    reset_send_flags(send_flags);

    char recv_flag = 0;

# if TRACE
    char label[MPC_OMP_TASK_LABEL_MAX_LENGTH + 1];
    memset(label, 0, sizeof(label));
# else  /* TRACE */
    char * label = NULL;
# endif

    #pragma omp parallel
    {
# if TRACE
        mpc_omp_task_trace_begin();
# endif /* TRACE */
        #pragma omp single
        {
            for (int k = 0; k < nt; k++)
            {
                if (block_rank[k*nt+k] == mype)
                {
                    SET_LABEL("potrf-%d", k);
                    mpc_omp_task(label, TASK_PRIORITY_POTRF(i, j, nt));
                    #pragma omp task depend(out: A[k][k]) firstprivate(k)
                    {
                        omp_potrf(A[k][k], ts, ts);
                    }

                    if (np != 1)
                    {
                        for (int dst = 0; dst < np; dst++)
                        {
                            int send_flag = 0;
                            for (int kk = k+1; kk < nt; kk++)
                            {
                                if (dst == block_rank[k*nt+kk]) { send_flag = 1; break; }
                            }
                            if (send_flag && dst != mype)
                            {
                                SET_LABEL("senddiag-%d-%d-%d", mype, dst, k*nt+k);
                                mpc_omp_task(label, TASK_PRIORITY_SEND(k, k, nt));
                                # pragma omp task depend(in: A[k][k]) untied
                                {
                                    MPI_Request req;
                                    MPI_Isend(A[k][k], ts*ts, MPI_DOUBLE, dst, k*nt+k, MPI_COMM_WORLD, &req);
                                    MPI_Wait(&req, MPI_STATUS_IGNORE);
                                }
                            }
                        }
                    }
                    reset_send_flags(send_flags);
                }

                if (block_rank[k*nt+k] != mype)
                {
                    for (int i = k + 1; i < nt; i++)
                    {
                        if (block_rank[k*nt+i] == mype) recv_flag = 1;
                    }
                    if (recv_flag)
                    {
                        SET_LABEL("recvdiag-%d-%d-%d", mype, block_rank[k*nt+k], k*nt+k);
                        mpc_omp_task(label, TASK_PRIORITY_RECV(i, j, nt));
                        #pragma omp task depend(out: B) firstprivate(k) untied
                        {
                            MPI_Request req;
                            MPI_Irecv(B, ts*ts, MPI_DOUBLE, block_rank[k*nt+k], k*nt+k, MPI_COMM_WORLD, &req);
                            MPI_Wait(&req, MPI_STATUS_IGNORE);
                        }
                        recv_flag = 0;
                    }
                }

                for (int i = k + 1; i < nt; i++)
                {
                    if (block_rank[k*nt+i] == mype)
                    {
                        SET_LABEL("trsm-%d-%d", k, i);
                        mpc_omp_task(label, TASK_PRIORITY_TRSM(i, j, nt));

                        if (block_rank[k*nt+k] == mype)
                        {
                            #pragma omp task depend(in: A[k][k]) depend(out: A[k][i]) firstprivate(k, i)
                            {
                                omp_trsm(A[k][k], A[k][i], ts, ts);
                            }
                        }
                        else
                        {
                            #pragma omp task depend(in: B) depend(out: A[k][i]) firstprivate(k, i)
                            {
                                omp_trsm(B, A[k][i], ts, ts);
                            }
                        }
                    }

                    if (block_rank[k*nt+i] == mype && np != 1)
                    {
                        for (int ii = k + 1; ii < i; ii++)
                        {
                            if (!send_flags[block_rank[ii*nt+i]]) send_flags[block_rank[ii*nt+i]] = 1;
                        }
                        for (int ii = i + 1; ii < nt; ii++)
                        {
                            if (!send_flags[block_rank[i*nt+ii]]) send_flags[block_rank[i*nt+ii]] = 1;
                        }
                        if (!send_flags[block_rank[i*nt+i]]) send_flags[block_rank[i*nt+i]] = 1;
                        for (int dst = 0; dst < np; dst++)
                        {
                            if (send_flags[dst] && dst != mype)
                            {
                                SET_LABEL("send-%d-%d-%d", mype, dst, k*nt+i);
                                mpc_omp_task(label, TASK_PRIORITY_SEND(k, i, nt));
                                #pragma omp task depend(in: A[k][i]) firstprivate(k, i, dst) untied
                                {
                                    MPI_Request req;
                                    MPI_Isend(A[k][i], ts*ts, MPI_DOUBLE, dst, k*nt+i, MPI_COMM_WORLD, &req);
                                    MPI_Wait(&req, MPI_STATUS_IGNORE);
                                }
                            }
                        }
                        reset_send_flags(send_flags);
                    }
                    if (block_rank[k*nt+i] != mype)
                    {
                        for (int ii = k + 1; ii < i; ii++)
                        {
                            if (block_rank[ii*nt+i] == mype) recv_flag = 1;
                        }
                        for (int ii = i + 1; ii < nt; ii++)
                        {
                            if (block_rank[i*nt+ii] == mype) recv_flag = 1;
                        }
                        if (block_rank[i*nt+i] == mype) recv_flag = 1;
                        if (recv_flag)
                        {
                            SET_LABEL("recv-%d-%d-%d", mype, block_rank[k*nt+i], k*nt+i);
                            mpc_omp_task(label, TASK_PRIORITY_RECV(i, j, nt));
                            #pragma omp task depend(out: C[i]) firstprivate(k, i) untied
                            {
                                MPI_Request req;
                                MPI_Irecv(C[i], ts*ts, MPI_DOUBLE, block_rank[k*nt+i], k*nt+i, MPI_COMM_WORLD, &req);
                                MPI_Wait(&req, MPI_STATUS_IGNORE);
                            }
                            recv_flag = 0;
                        }
                    }
                }

                for (int i = k + 1; i < nt; i++)
                {
                    for (int j = k + 1; j < i; j++)
                    {
                        if (block_rank[j*nt+i] == mype)
                        {
                            SET_LABEL("gemm-%d-%d", i, k);
                            if (block_rank[k*nt+i] == mype && block_rank[k*nt+j] == mype)
                            {
                                mpc_omp_task(label, TASK_PRIORITY_GEMM(i, j, nt));
                                #pragma omp task depend(in: A[k][i], A[k][j]) depend(out: A[j][i]) firstprivate(k, j, i)
                                {
                                    omp_gemm(A[k][i], A[k][j], A[j][i], ts, ts);
                                }
                            }
                            else if (block_rank[k*nt+i] != mype && block_rank[k*nt+j] == mype)
                            {
                                mpc_omp_task(label, TASK_PRIORITY_GEMM(i, j, nt));
                                #pragma omp task depend(in: C[i], A[k][j]) depend(out: A[j][i]) firstprivate(k, j, i)
                                {
                                    omp_gemm(C[i], A[k][j], A[j][i], ts, ts);
                                }
                            }
                            else if (block_rank[k*nt+i] == mype && block_rank[k*nt+j] != mype)
                            {
                                mpc_omp_task(label, TASK_PRIORITY_GEMM(i, j, nt));
                                #pragma omp task depend(in: A[k][i], C[j]) depend(out: A[j][i]) firstprivate(k, j, i)
                                {
                                    omp_gemm(A[k][i], C[j], A[j][i], ts, ts);
                                }
                            }
                            else
                            {
                                mpc_omp_task(label, TASK_PRIORITY_GEMM(i, j, nt));
                                #pragma omp task depend(in: C[i], C[j]) depend(out: A[j][i]) firstprivate(k, j, i)
                                {
                                    omp_gemm(C[i], C[j], A[j][i], ts, ts);
                                }
                            }
                        }
                    }

                    if (block_rank[i*nt+i] == mype)
                    {
                        SET_LABEL("syrk-%d-%d", i, k);
                        if (block_rank[k*nt+i] == mype)
                        {
                            mpc_omp_task(label, TASK_PRIORITY_SYRK(i, j, nt));
                            #pragma omp task depend(in: A[k][i]) depend(out: A[i][i]) firstprivate(k, i)
                            {
                                omp_syrk(A[k][i], A[i][i], ts, ts);
                            }
                        }
                        else
                        {
                            mpc_omp_task(label, TASK_PRIORITY_SYRK(i, j, nt));
                            #pragma omp task depend(in: C[i]) depend(out: A[i][i]) firstprivate(k, i)
                            {
                                omp_syrk(C[i], A[i][i], ts, ts);
                            }
                        }
                    }
                }
            } // for k
        }// pragma omp single
# if TRACE
        mpc_omp_task_trace_end();
# endif /* TRACE */
    }// pragma omp parallel

    free(send_flags);
    MPI_Barrier(MPI_COMM_WORLD);
}

