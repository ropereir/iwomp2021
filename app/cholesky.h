
#include <math.h>
#include <omp.h>
#include <mkl.h>

#include <sys/time.h>
#include <sys/times.h>


//    dpotrf_(&L, pp_ts, pp_A, pp_ts, &INFO);

//extern void dpotrf_ (const char *, int *, const double *, int *, int *);
/*
void dpotrf_ (const char* uplo, const MKL_INT* n, double* a,
              const MKL_INT* lda, MKL_INT* info );
*/

/*
void dgemm_ (const char *transa, const char *transb, int *l, int *n, int *m, double *alpha,
             const void *a, int *lda, void *b, int *ldb, double *beta, void *c, int *ldc);

void dtrsm_ (char *side, char *uplo, char *transa, char *diag, int *m, int *n, double *alpha,
             double *a, int *lda, double *b, int *ldb);

void dsyrk_ (char *uplo, char *trans, int *n, int *k, double *alpha, double *a, int *lda,
             double *beta, double *c, int *ldc);
*/
void dtrmm_ (char *side, char *uplo, char *transa, char *diag, int *m, int *n, double *alpha,
             double *a, int *lda, double *b, int *ldb);

enum blas_order_type {
            blas_rowmajor = 101,
            blas_colmajor = 102 };

enum blas_cmach_type {
            blas_base      = 151,
            blas_t         = 152,
            blas_rnd       = 153,
            blas_ieee      = 154,
            blas_emin      = 155,
            blas_emax      = 156,
            blas_eps       = 157,
            blas_prec      = 158,
            blas_underflow = 159,
            blas_overflow  = 160,
            blas_sfmin     = 161};

enum blas_norm_type {
            blas_one_norm       = 171,
            blas_real_one_norm  = 172,
            blas_two_norm       = 173,
            blas_frobenius_norm = 174,
            blas_inf_norm       = 175,
            blas_real_inf_norm  = 176,
            blas_max_norm       = 177,
            blas_real_max_norm  = 178 };

void add_to_diag_hierarchical (double ** matrix, const int ts, const int nt, const float alpha)
{
	for (int i = 0; i < nt * ts; i++)
		matrix[(i/ts) * nt + (i/ts)][(i%ts) * ts + (i%ts)] += alpha;
}

void add_to_diag(double * matrix, const int n, const double alpha)
{
	for (int i = 0; i < n; i++)
		matrix[ i + i * n ] += alpha;
}

float get_time()
{
	static double gtod_ref_time_sec = 0.0;

	struct timeval tv;
	gettimeofday(&tv, NULL);

	// If this is the first invocation of through dclock(), then initialize the
	// "reference time" global variable to the seconds field of the tv struct.
	if (gtod_ref_time_sec == 0.0)
		gtod_ref_time_sec = (double) tv.tv_sec;

	// Normalize the seconds field of the tv struct so that it is relative to the
	// "reference time" that was recorded during the first invocation of dclock().
	const double norm_sec = (double) tv.tv_sec - gtod_ref_time_sec;

	// Compute the number of seconds since the reference time.
	const double t = norm_sec + tv.tv_usec * 1.0e-6;

	return (float) t;
}

void initialize_matrix(const int n, double *matrix)
{
	int ISEED[4] = {0,0,0,1};
	int intONE=1;

#ifdef VERBOSE
	printf("Initializing matrix with random values ...\n");
#endif

	for (int i = 0; i < n*n; i+=n) {
		dlarnv_(&intONE, &ISEED[0], &n, &matrix[i]);
	}
/*
	for (int i=0; i<n; i++) {
		for (int j=0; j<n; j++) {
			matrix[j*n + i] = matrix[j*n + i] + matrix[i*n + j];
			matrix[i*n + j] = matrix[j*n + i];
		}
	}
*/
	add_to_diag(matrix, n, (double) n);
}

void initialize_tile(const int ts, double *tile)
{
	int ISEED[4] = {0,0,0,1};
	int intONE=1;

#ifdef VERBOSE
	printf("Initializing matrix with random values ...\n");
#endif

  int n = ts*ts;
	for (int i = 0; i < ts*ts; i+=n) {
		dlarnv_(&intONE, &ISEED[0], &n, &tile[i]);
	}
}
