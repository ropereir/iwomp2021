# Prerequisities

## Cholesky blocked matrix factorization
Forked from https://github.com/devreal/cholesky_omptasks/

## MPC
You need an install version of MPC on `cea/these/pereirar` branch`.

```bash
export MPC=realpath(mpc)
export MPC_BRANCH=cea/these/pereirar
export MPC_BUILD_DIR=$MPC/build/$MPC_BRANCH
export MPC_PREFIX=$MPC-install/build/$MPC_BRANCH
git clone --branch $MPC_BRANCH $MPC
mkdir -p $MPC_BUILD_DIR $MPC_PREFIX
cd $MPC_BUILD_DIR
$MPC/installmpc --prefix=$MPC_PREFIX
source $MPC_PREFIX/mpcvars.sh
```

## MKL
Please refer to https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library.html

# Compile
```bash
make
```

# Run
```bash
mpcrun -N=1 -p=1 -n=1 -c=4 ./a.out-fifo 4096 512 0
```

You may want to set `-N=`, `-p=`, `-n=`, `-c=`, `OMP_NUM_THREADS=`, and the app parameters to your conveniance.    
To trace execution, please set `MPCFRAMEWORK_OMP_TASK_TRACE=true`and enable the `-DTRACE` inside the Makefile
To modify priotization strategy, please set `MPCFRAMEWORK_OMP_TASK_PRIORITYPOLICY=X`

| MPCFRAMEWORK_OMP_TASK_PRIORITYPOLICY | Prioritization |
|:------------------------------------:|:--------------:|
|                   0                  |      FIFO      |
|                   1                  |       MA1      |
|                   2                  |       MA2      |
|                   3                  |       SA1      |
|                   4                  |       SA2      |
|                   5                  |       FA1      |

To view more parameters, please run :
```bash
mpc_print_config --conf | grep TASK
```
