# `app` folder
see `app/README` - forked from https://github.com/devreal/cholesky_omptasks

# `mpi-omp-spreading` folder
see `mpi-omp-spreading/README`

# `weak-scaling` folder
see `weak-scaling/README`
