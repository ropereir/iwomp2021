#!/bin/bash

# NUMBER OF MEASURES PER POINT
ITER0=0
ITERN=20

# SLURM TIMEOUT
TIME='00:10:00'

for partition in 'sklb' ; do
    for policy in '0 fifo a.out-fifo' '1 ma1 a.out-ma1' '2 ma2 a.out-ma2' '3 sa1 a.out-sa1' '4 sa2 a.out-sa2' '5 fa1 a.out-fa1'; do
        a=( $policy )
        priority=${a[0]}
        mode=${a[1]}
        binary=${a[2]}
        for block in "512" ; do
            for repartition in "1 1 1 24 24 41286 {0:24}" "1 1 2 48 24 52015" "2 2 4 48 24 65536" "4 4 8 48 24 82570" "8 8 16 48 24 104031" "16 16 32 48 24 131072"; do
                a=( $repartition )
                N=${a[0]}
                p=${a[1]}
                n=${a[2]}
                c=${a[3]}
                oc=${a[4]}
                matrix=${a[5]}
                places=${a[6]}
                for (( i=$ITER0; i < $ITERN; i++ )) ; do
                    dir=$partition/$mode/$N-$p-$n-$c-$oc-$matrix-$block-$i
                    mkdir -p $dir
                    if ! test -f $dir/out ; then
                        echo "Running $dir"
                        echo "OMP_PLACES=$places MPCFRAMEWORK_OMP_TASK_PRIORITYPOLICY=$priority OMP_NUM_THREADS=$oc mpcrun -v -N=$N -p=$p -n=$n -c=$c --opt=\"-p $partition --exclusive -t $TIME\" command time -v ../app/$binary $matrix $block 0 > $dir/out 2> $dir/err" > $dir/cmd
                        OMP_PLACES=$places MPCFRAMEWORK_OMP_TASK_PRIORITYPOLICY=$priority OMP_NUM_THREADS=$oc mpcrun -v -N=$N -p=$p -n=$n -c=$c --opt="-p $partition --exclusive -t $TIME" command time -v ../app/$binary $matrix $block 0 > $dir/out 2> $dir/err &
                    else
                        echo "$dir already exists"
                    fi
                done
            done
        done
    done
done
