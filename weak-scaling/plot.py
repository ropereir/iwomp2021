import csv
import json
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

if len(sys.argv) != 2:
    print("usage: {} [data.csv]".format(sys.argv[0]))
    sys.exit(1)

matplotlib.rcParams.update({'font.size': 20})

samples         = 20 
approaches      = ('fifo', 'ma1', 'sa2', 'fa1')
labels          = ('FIFO', 'MA-1', 'SA', 'FA')
linestyles      = ('dotted', 'dashed', 'dashdot', (0, (5, 10)), (0, (3, 1, 1, 1, 1, 1)))
#repartitions = ((1, 65536), (2, 82570), (4, 104032), (8, 131072), (16, 165140), (32, 208063))
repartitions = ((1, 41286), (2, 52015), (4, 65536), (8, 82570), (16, 104031), (32, 131072)) #, (32, 165140))
x = [i for i in range(len(repartitions))]
# samples = 3 

# parse
# repartition:approach:i:n:p:rank:gflops:total:compute
df = pd.read_csv(sys.argv[1], sep=':')
MIN = 0
MAX = 1
MED = 2
y = np.empty((3, len(repartitions)))
for i in range(len(approaches)):
    approach = approaches[i]
    xi = 0
    print(approach)
    for repartition in repartitions:
        n = repartition[0]
        matrix = repartition[1]
        d = df[df['approach'] == approach]
        d = d[d['n'] == n]
        d = d[d['matrix'] == matrix]
        print('    repartitions {} : {}/{} samples'.format(repartition, len(d), n * samples))
#        assert(len(d) == N * samples)
        d = d.groupby(['i']).max('total')
        y[MIN][xi] = d['total'].min()
        y[MAX][xi] = d['total'].max()
        y[MED][xi] = d['total'].median()
        xi = xi + 1

    # convert raw time to weak-scaling factor
    f = y[MED][0]
    print(y[MED])
    y[MIN] = f / y[MIN]
    y[MAX] = f / y[MAX] 
    y[MED] = f / y[MED] 

    # plot
#    yerr = (y[MED] - y[MAX], y[MIN] - y[MED])
#    yerr = (y[MED] - y[MIN], y[MAX] - y[MED])
    yerr = None
    plt.errorbar(x, y[MED], yerr, label=labels[i], linestyle=linestyles[i], linewidth=4.0)
#    plt.fill_between(x, y[MAX], y[MIN], label=approach, interpolate=True, alpha=0.2)

# labels
xticks          = x
xticks_name     = ['{}-{}'.format(repartition[0], repartition[1]) for repartition in repartitions]
plt.xticks(x, xticks_name)
plt.ylim(bottom=0)

plt.ylabel('Weak-scaling efficiency')
plt.text(x[len(x) // 2] - 0.5, -0.1, 'MPI processes - Matrix size', ha='center', va='bottom', rotation=0)
plt.suptitle('Weak scaling on a Cholesky factorisation with blocks of size 512\nMPI processes with 24 OMP threads per rank (Skylake sockets)\n')

# plt.axhline(y=compute_time[0] + 0.5, color='b', linestyle=':')
plt.hlines(1.0, 0, len(repartitions) - 1, color='black')

# legend
leg = plt.legend(loc='lower left', fontsize=24)
for legobj in leg.legendHandles:
    legobj.set_linewidth(10.0)
order = [3, 4, 1, 2, 0]
plt.legend([leg.legendHandles[idx] for idx in order], [labels[idx] for idx in order])

# show
plt.show()
