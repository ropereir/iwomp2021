#!/bin/bash
echo "partition:approach:N:p:n:c:oc:i:matrix:block:rank:gflops:total:compute"

for partition in 'sklb' ; do
    for policy in '0 fifo a.out-1' '1 ma1 a.out-1' '2 ma2 a.out-1' '3 sa1 a.out-NT-I-J' '4 sa2 a.out-INT_MAX' '5 fa1 a.out-1'; do
        a=( $policy )
        priority=${a[0]}
        mode=${a[1]}
        binary=${a[2]}
        for block in "512" ; do
            for repartition in "1 1 1 24 24 41286" "1 1 2 48 24 52015" "2 2 4 48 24 65536" "4 4 8 48 24 82570" "8 8 16 48 24 104031" "16 16 32 48 24 131072"; do
                a=( $repartition )
                N=${a[0]}
                p=${a[1]}
                n=${a[2]}
                c=${a[3]}
                oc=${a[4]}
                matrix=${a[5]}
                for i in {0..19} ; do
                    dir=$partition/$mode/$N-$p-$n-$c-$oc-$matrix-$block-$i
                    mkdir -p $dir
                    if test -f $dir/out ; then
                        for line in $(cat "$dir/out" | grep mype | cut -d ':' -f 4,12,14,16 | tr -d ' ') ; do
                            echo -n "$partition:$mode:$N:$p:$n:$c:$oc:$i:$matrix:$block:"
                            echo $line
                        done
                    fi
                done
            done
        done
    done
done
