`run.sh` - this file needs to be edited (slurm partitions, cores configurations...) - output results to files

`to_csv.sh` - convert output to a csv data file format

`python3 plot.py data.csv` - to visualize results
