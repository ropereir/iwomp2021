`run.sh` - lance les executions avec prise de traces MPC

`stats.sh` - converti les traces MPC en statistique via le post-traitement python disponible dans `MPC/MPC_OpenMP/tools/`

`python3 plot.py` - pour tracer les courbes

