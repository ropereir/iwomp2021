import json
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys

if len(sys.argv) != 1:
    print("{}".format(sys.argv[0]))
    sys.exit(1)

matplotlib.rcParams.update({'font.size': 18})

N = 16
m = 131072
block_size = 512
partition = 'sklb'

approaches      = ('fifo', 'ma1', 'ma2', 'sa2', 'fa1') #, 'fa2')
labels          = ('FIFO', 'MA-1', 'MA-2', 'SA', 'FA')

repartitions = ((16, 48), (32, 24), (64, 12), (128, 6)) #, (256, 3))
n_samples = 20

compute_time            = np.empty((n_samples, len(approaches) * len(repartitions)))
communication_time      = np.empty((n_samples, len(approaches) * len(repartitions)))
famine_time             = np.empty((n_samples, len(approaches) * len(repartitions)))
famine_block_time       = np.empty((n_samples, len(approaches) * len(repartitions)))
famine_unblock_time     = np.empty((n_samples, len(approaches) * len(repartitions)))
famine_block_percents   = np.empty((n_samples, len(approaches) * len(repartitions)))
famine_unblock_percents = np.empty((n_samples, len(approaches) * len(repartitions)))
overhead_time           = np.empty((n_samples, len(approaches) * len(repartitions)))

MIN = 0
MAX = 1
MED = 2
in_task     = np.empty((3, len(approaches) * len(repartitions)))
famine      = np.empty((3, len(approaches) * len(repartitions)))
overhead    = np.empty((3, len(approaches) * len(repartitions)))
total       = np.empty((3, len(approaches) * len(repartitions)))

# parse data
for i in range(len(repartitions)):
    for j in range(len(approaches)):
        samples = []
        idx = i * len(approaches) + j
        for k in range(0, n_samples):
            n = 1 << i
            nmpi = 16 * n
            nomp = 48 / n
            f = '{}/{}/{}-{}-16-16-{}-{}/traces.stats'.format(partition, approaches[j], m, block_size, nmpi, k)
            print('loading {}'.format(f))
            stats = json.load(open(f))

            total_time                  = stats['time']['flat (s.)']['max-process-time']
            compute_percent             = stats['time']['proportion (%)']['in-task']['compute'] / 100.0
            communication_percent       = stats['time']['proportion (%)']['in-task']['communication']['total'] / 100.0
            famine_percent              = stats['time']['proportion (%)']['out-task']['famine']['total'] / 100.0
            famine_block_percent        = stats['time']['proportion (%)']['out-task']['famine']['blocked'] / 100.0
            famine_unblock_percent      = stats['time']['proportion (%)']['out-task']['famine']['unblocked'] / 100.0
            overhead_percent            = stats['time']['proportion (%)']['out-task']['overhead'] / 100.0

            compute_time[k][idx]            = total_time * compute_percent
            communication_time[k][idx]      = total_time * communication_percent
            famine_time[k][idx]             = total_time * famine_percent
            famine_block_time[k][idx]       = total_time * famine_block_percent
            famine_unblock_time[k][idx]     = total_time * famine_unblock_percent
            famine_block_percents[k][idx]   = famine_block_percent
            famine_unblock_percents[k][idx] = famine_unblock_percent
            overhead_time[k][idx]           = total_time * overhead_percent

            samples.append((k, total_time))

        samples = sorted(samples, key=lambda x: x[1])

        # minimum
        ks = {}
        ks[MIN] = 0
        ks[MAX] = len(samples) - 1
        ks[MED] = len(samples) // 2
        for l in (MIN, MAX, MED):
            k = samples[ks[l]][0]
            in_task[l][idx]     = compute_time[k][idx] + communication_time[k][idx]
            famine[l][idx]      = famine_time[k][idx]
            overhead[l][idx]    = overhead_time[k][idx]
            total[l][idx]       = samples[ks[l]][1]

# plot
width   = 0.2
offset  = width * 0.1
x       = [(i * len(approaches) + j) * (width + offset) + i * width for i in range(len(repartitions)) for j in range(len(approaches))]

legend = ('in tasks', 'idle', 'overhead')
bars = plt.bar(x, in_task[MED], width, color='#85a0d2')
plt.bar(x, famine[MED],     width, bottom=in_task[MED], color='#dede4b')
plt.bar(x, overhead[MED],   width, bottom=in_task[MED] + famine[MED], yerr=(total[MED] - total[MIN], total[MAX] - total[MED]), color='#c79b7d')

#for i, rect in enumerate(bars):
#    plt.text(rect.get_x() + rect.get_width() / 2.0, 7, '{}%'.format(int(in_task[MED][i] / total[MED][i] * 100.0)), ha='center', va='bottom', rotation=90)

for i in range(len(x)):
    plt.text(x[i], -8, labels[i % len(approaches)], ha='center', va='bottom', rotation=90)

for i in range(len(repartitions)):
    r = repartitions[i]
    plt.text(x[len(approaches) * i + len(approaches) // 2], -14, '{}-{}'.format(r[0], r[1]), ha='center', va='bottom', rotation=0)

plt.text(x[len(approaches) * len(repartitions) // 2], -20, 'MPI-OMP cores spreading', ha='center', va='bottom', rotation=0)

# labels
s = int(len(approaches) / 2)
xticks          = x[s::len(approaches)]
# xticks_name     = ['{}-{}'.format(repartitions[i][0], repartitions[i][1]) for i in range(len(repartitions))]
xticks_name     = [''] * len(repartitions)
plt.xticks(xticks, xticks_name)

plt.ylabel('Time (in sec.)')
#plt.xlabel('MPI-OMP cores spreading')
plt.suptitle('Cholesky factorisation time based on MPI/OMP cores spreading\non {} Skylake nodes with n={}, block_size={}'.format(N, m, block_size))
plt.legend(legend, loc="upper left")

# plt.axhline(y=compute_time[0] + 0.5, color='b', linestyle=':')
times = famine[MED]# sum((in_task[MED], famine[MED], overhead[MED]))
for i in range(len(repartitions)):
    repartition = repartitions[i]
    print('{} - {}'.format(repartition, times[len(approaches)*i:len(approaches)*(i+1):]))

plt.show()
