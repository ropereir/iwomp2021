#!/bin/bash

# NUMBER OF MEASURES PER POINT
ITER0=0
ITERN=20

for partition in 'sklb' ; do
    for policy in '0 fifo a.out-fifo' '1 ma1 a.out-ma1' '2 ma2 a.out-ma2' '3 sa1 a.out-sa1' '4 sa2 a.out-sa2' '5 fa1 a.out-fa1'; do
        a=( $policy )
        priority=${a[0]}
        mode=${a[1]}
        binary=${a[2]}
        for matrix in "512 131072" ; do
            a=( $matrix )
            block=${a[0]}
            m=${a[1]}
            for repartition in "16 16 16 48 48" "16 16 32 48 24" "16 16 64 48 12" "16 16 128 48 6"; do
                for (( i=$ITER0; i < $ITERN; i++ )) ; do
                    a=( $repartition )
                    N=${a[0]}
                    p=${a[1]}
                    n=${a[2]}
                    c=${a[3]}
                    t=${a[4]}
                    traces=$partition/$mode/$m-$block-$N-$p-$n-$i/traces
                    mkdir -p $traces
                    if ! test -f $trace.stats -o -s $traces.stats ; then
                        echo "running $traces"
                        rm -rf $traces/*
                        echo "MPCFRAMEWORK_OMP_TASK_PRIORITYPOLICY=$priority MPCFRAMEWORK_OMP_TASK_TRACE=true OMP_NUM_THREADS=$t mpcrun -v -N=$N -p=$p -n=$n -c=$c --opt=\"-p $partition --exclusive -t 00:02:30\" ../app/$binary $m $block 0 > $traces.out 2> $traces.err" > $traces.cmd
                       MPCFRAMEWORK_OMP_TASK_PRIORITYPOLICY=$priority MPCFRAMEWORK_OMP_TASK_TRACE=true OMP_NUM_THREADS=$t mpcrun -v -N=$N -p=$p -n=$n -c=$c --opt="-p $partition --exclusive -t 00:02:30" ../app/$binary $m $block 0 > $traces.out 2> $traces.err
                        mv *.dat $traces && srun -n 1 -N 1 -c 8 -p sandy ./stats.sh $traces $traces.stats &
                    else
                        echo "$traces already exists"
                    fi
                done
            done
        done
    done
done
